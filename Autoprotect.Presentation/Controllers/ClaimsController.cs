﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autoprotect.Core.Services;

namespace Autoprotect.Presentation.Controllers
{
    public class ClaimsController : Controller
    {
        private readonly IClaimsService _claimsService;

        public ClaimsController(IClaimsService claimsService)
        {
            _claimsService = claimsService;
        }

        // GET: Claims
        public ActionResult Index()
        {
            return View();
        }

        // GET: Claims/Details/5
        public ActionResult Details(int id)
        {
            var findClaim = _claimsService.FindClaim(id);

            return View(findClaim);
        }

     
    }
}
