﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;

namespace DZR.EVzorci.WebApi2.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion


        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            //container.RegisterType(ICla)
//            container.AddExtension(new MssqlModule(@"Data Source=FRANKUNDERWOOD\R2SQLEXPRESS;Initial Catalog=DZR.EVzorci.DB;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False"));
//            container.RegisterType<IUserStore<ApplicationUser, int>, EvUserStore>();
//            container.RegisterType<IUserManagerFactory, UserManagerFactory>();
//            container.RegisterType<ISampleService, SampleService>();
//            container.AddExtension(new ExternalPatientModule());
        }
    }
}
