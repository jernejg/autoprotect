﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Autoprotect.Presentation.Startup))]
namespace Autoprotect.Presentation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
