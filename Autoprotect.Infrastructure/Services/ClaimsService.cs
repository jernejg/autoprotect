﻿using System;
using System.CodeDom;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Autoprotect.Core.Entities;
using Autoprotect.Core.Services;

namespace Autoprotect.Infrastructure.Services
{
    public class ClaimsService : IClaimsService, IDisposable
    {
        private readonly HttpClient _httpClient;

        public ClaimsService()
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri("http://localhost:1712/")
            };
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }


        public async Task<Claim> FindClaim(int id)
        {
            var response = await _httpClient.GetAsync(string.Format("Claims/Details/{0}", id));
            Claim result = null;

            if (response.IsSuccessStatusCode)
                result = await response.Content.ReadAsAsync<Claim>();

            return result;
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}