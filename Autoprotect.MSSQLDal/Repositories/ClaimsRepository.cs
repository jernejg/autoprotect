﻿using System;
using Autoprotect.Core.Entities;
using Autoprotect.Core.Repositories;

namespace Autoprotect.MSSQLDal.Repositories
{
    public class ClaimsRepository : IClaimsRepository
    {
        private readonly simplesolutionEntities _contex = new simplesolutionEntities();

        public Claim FindClaim(int id)
        {
            return _contex.Claims.Find(id);
        }
    }
}
