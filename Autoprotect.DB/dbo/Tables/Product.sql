﻿CREATE TABLE [dbo].[Product] (
    [ID]          INT           NOT NULL,
    [Description] NVARCHAR (50) NULL,
    [Duration]    SMALLINT      NULL,
    [ClaimLimit]  REAL          NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ID] ASC)
);

