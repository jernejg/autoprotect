﻿CREATE TABLE [dbo].[PolicyHolder] (
    [ID]            INT           NOT NULL,
    [FullName]      NVARCHAR (50) NULL,
    [DOB]           DATE          NULL,
    [ContactNumber] NVARCHAR (10) NULL,
    CONSTRAINT [PK_PolicyHolder] PRIMARY KEY CLUSTERED ([ID] ASC)
);

