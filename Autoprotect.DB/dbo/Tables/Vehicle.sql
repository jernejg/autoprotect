﻿CREATE TABLE [dbo].[Vehicle] (
    [ID]           INT           NOT NULL,
    [Registration] NVARCHAR (10) NULL,
    [Make]         NVARCHAR (50) NULL,
    [Model]        NVARCHAR (50) NULL,
    [Mileage]      INT           NULL,
    [Value]        INT           NULL,
    CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED ([ID] ASC)
);

