﻿CREATE TABLE [dbo].[Claim] (
    [ID]        INT  NOT NULL,
    [PolicyID]  INT  NULL,
    [ClaimDate] DATE NULL,
    [Amount]    REAL NULL,
    CONSTRAINT [PK_Claim] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Claim_Claim] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[Policy] ([ID])
);

