﻿CREATE TABLE [dbo].[Policy] (
    [ID]             INT      NOT NULL,
    [ProductID]      INT      NULL,
    [VehicleID]      INT      NULL,
    [PolicyHolderID] INT      NULL,
    [StateDate]      DATETIME NULL,
    CONSTRAINT [PK_Policy] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Policy_Policy] FOREIGN KEY ([ID]) REFERENCES [dbo].[Policy] ([ID]),
    CONSTRAINT [FK_Policy_PolicyHolder] FOREIGN KEY ([PolicyHolderID]) REFERENCES [dbo].[PolicyHolder] ([ID]),
    CONSTRAINT [FK_Policy_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Product] ([ID]),
    CONSTRAINT [FK_Policy_Vehicle] FOREIGN KEY ([VehicleID]) REFERENCES [dbo].[Vehicle] ([ID])
);

