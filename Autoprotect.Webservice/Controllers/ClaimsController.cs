﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using Autoprotect.Core.Entities;
using Autoprotect.Core.Repositories;

namespace Autoprotect.Webservice.Controllers
{
    public class ClaimsController : Controller
    {
        private readonly IClaimsRepository _repo;

        public ClaimsController(IClaimsRepository repo)
        {
            _repo = repo;
        }

        /// <summary>
        /// Best practice here would be not to directly call the repository from the controller but use
        /// an extra business layer that would map entities to dto's before returning them.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Claim> Details(int id)
        {
            var claim = await _repo.FindClaim(id);

            throw new NotImplementedException();
            //return Request.CreateResponse(HttpStatusCode.OK, claim);
        }

      
    }
}
