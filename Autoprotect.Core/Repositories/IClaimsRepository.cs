﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autoprotect.Core.Entities;

namespace Autoprotect.Core.Repositories
{
    public interface IClaimsRepository
    {
         Task<Claim> FindClaim(int id);
        
    }


}
