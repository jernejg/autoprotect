﻿using System;
using System.Collections.Generic;

namespace Autoprotect.Core.Entities
{
    public  class Product
    {
        public Product()
        {
            this.Policies = new HashSet<Policy>();
        }

        public int ID { get; set; }
        public string Description { get; set; }
        public Nullable<short> Duration { get; set; }
        public Nullable<float> ClaimLimit { get; set; }

        public virtual ICollection<Policy> Policies { get; set; }
    }
}