﻿using System;
using System.Collections.Generic;

namespace Autoprotect.Core.Entities
{
    public  class Policy
    {
        public Policy()
        {
            this.Claims = new HashSet<Claim>();
        }

        public int ID { get; set; }
        public Nullable<int> ProductID { get; set; }
        public Nullable<int> VehicleID { get; set; }
        public Nullable<int> PolicyHolderID { get; set; }
        public Nullable<System.DateTime> StateDate { get; set; }

        public virtual ICollection<Claim> Claims { get; set; }
        public virtual Policy Policy1 { get; set; }
        public virtual Policy Policy2 { get; set; }
        public virtual PolicyHolder PolicyHolder { get; set; }
        public virtual Product Product { get; set; }
        public virtual Vehicle Vehicle { get; set; }
    }
}