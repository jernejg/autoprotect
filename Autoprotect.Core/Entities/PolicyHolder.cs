﻿using System;
using System.Collections.Generic;

namespace Autoprotect.Core.Entities
{
    public  class PolicyHolder
    {
        public PolicyHolder()
        {
            this.Policies = new HashSet<Policy>();
        }

        public int ID { get; set; }
        public string FullName { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string ContactNumber { get; set; }

        public virtual ICollection<Policy> Policies { get; set; }
    }
}