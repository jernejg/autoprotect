﻿using System;
using System.Collections.Generic;

namespace Autoprotect.Core.Entities
{
    public  class Vehicle
    {
        public Vehicle()
        {
            this.Policies = new HashSet<Policy>();
        }

        public int ID { get; set; }
        public string Registration { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public Nullable<int> Mileage { get; set; }
        public Nullable<int> Value { get; set; }

        public virtual ICollection<Policy> Policies { get; set; }
    }
}