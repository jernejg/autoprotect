﻿using System;

namespace Autoprotect.Core.Entities
{
    public  class Claim
    {
        public int ID { get; set; }
        public Nullable<int> PolicyID { get; set; }
        public Nullable<System.DateTime> ClaimDate { get; set; }
        public Nullable<float> Amount { get; set; }

        public virtual Policy Policy { get; set; }
    }
}
