﻿using System.Threading.Tasks;
using Autoprotect.Core.Entities;

namespace Autoprotect.Core.Services
{
    public interface IClaimsService
    {
        Task<Claim> FindClaim(int id);

    }
}